package server;
// HolaMonRmiO.java
/**
 * Aquest és l'objecte remot per a la version RMI del missatge
 * de salutació "Hola Món"
 */       

import java.rmi.*;
import java.rmi.server.*;

import interficie.HolaMonRmiI;
public class HolaMonRmiO extends UnicastRemoteObject

implements HolaMonRmiI {
  public HolaMonRmiO() throws RemoteException { // constructor
    // super(); // certament aquesta línia no és necessària (per defecte)
  }
  
  public String objRemotHola(String client) throws RemoteException {
    return("Hola "+client);
  }
}
