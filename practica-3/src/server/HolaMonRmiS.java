package server;
// HolaMonRmiS.java
/**
 * Aquest programa és el servidor remot per a la versió RMI del missatge
 * de salutació "Hola Món"
 */             
import java.rmi.*;

public class HolaMonRmiS {
  public static void main(String args[]) {
    try {
      HolaMonRmiO objRemot= new HolaMonRmiO();
      Naming.rebind("holastring",objRemot);
      System.out.println("Objecte remot preparat");
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
}
