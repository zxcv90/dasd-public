#!/bin/bash
#Localitzar el classpath (directori on tenim tot, que es el direc. actual)
if [ $# -lt 2 ]
then
	echo "Falten paràmetres. Exemple : ./obrirClient.sh localhost 45 "
	exit	
fi
a=`pwd`

#Generar el policy per al Client:
rm client.policy 2> /dev/null
echo "grant codeBase \"file://$a\" {" > client.policy
echo "permission java.security.AllPermission;" >> client.policy
echo "};" >> client.policy

java -cp . -Djava.rmi.server.codebase=file://$a -Djava.security.policy=client.policy client.ComputePi $1 $2
