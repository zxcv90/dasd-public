
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Echo {
  private static EchoObject ss;
  public static void main(String[] args) {

	  ss = new EchoObject();

    BufferedReader stdIn= new BufferedReader(new InputStreamReader(System.in));
    PrintWriter stdOut= new PrintWriter(System.out);
    String input,output;
    try {
      while (true){
    	  stdOut.print("> "); stdOut.flush();
    	  Scanner in=new Scanner(System.in);
    	  input=in.nextLine();
    	  System.out.println(ss.echo(input));
      }

    } catch (IOException e) {}
  }
}
