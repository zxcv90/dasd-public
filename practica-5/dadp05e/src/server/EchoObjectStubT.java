package server;
import java.net.*;
import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

public class EchoObjectStubT implements EchoInt {
private Socket echoSocket= null;
private PrintWriter os= null;
private BufferedReader is= null;
private String host= "localhost";
private int port= 7;
private String output= "Error";
private boolean connectat= false;

  Timeout tout= null;
  public void setHostAndPort(String host, int port) {
    this.host= host;
    this.port= port;
    tout = new Timeout(10,this);
  }

  public String echo(String input) throws java.rmi.RemoteException {
    connecta();
    if (echoSocket != null && os != null && is != null) {
      try {
        os.println(input);
        os.flush();
        output= is.readLine();
      } catch (IOException e) {
        System.err.println("Excepció llegint/escrivint socket");
      }
    }
    programaDesconnexio();
    return output;
  }

  private synchronized void connecta() throws java.rmi.RemoteException {
    try {
      if (!connectat) {
        echoSocket= new Socket(host,port);
        is= new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
        os= new PrintWriter(echoSocket.getOutputStream());
        connectat= true;
        System.out.println("Connectat al server");
        }
    }catch (Exception e){
      throw new java.rmi.RemoteException();
    }
  }

  private synchronized void desconnecta() {
    try {
      os.close();
      is.close();
      echoSocket.close();
      connectat= false;
      System.out.println("Desconnectat del server");
    } catch (IOException e) {
      System.err.println("Error tancant socket");
    }
  }

  private synchronized void programaDesconnexio() {
    tout.start();
  }

  class Timeout {
    Timer timer;
    EchoObjectStubT stub;
    int segons;

    public Timeout (int segons, EchoObjectStubT stub) {
      this.segons= segons;
      this.stub= stub;
    }

    public void start() {
      timer = new Timer();
      timer.schedule(new TimeoutTask(), segons);
    }

    public void cancel() {
       timer.cancel();
    }

    class TimeoutTask extends TimerTask {
      public void run() {
        System.out.format("Timer Task Finished..!%n");
        desconnecta(); //Disconnect
        timer.cancel(); //Cancel timer
      }
    }
  }
}
